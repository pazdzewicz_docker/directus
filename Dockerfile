ARG DIRECTUS_VERSION="10.7.2"

FROM directus/directus:${DIRECTUS_VERSION}

ENV DIRECTUS_VERSION ${DIRECTUS_VERSION}

USER root

RUN apk update && \
    apk add bash

USER node