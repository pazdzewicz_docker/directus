#!/bin/bash
NPX="$(which npx)"
FIND="$(which find)"

MIGRATION_FOLDER="/migrations/"
MIGRATION_FILE="${1}"
MIGRATION_PATH="${MIGRATION_FOLDER}/${MIGRATION_FILE}"

FIND_SEARCH="migration_*.yml"

if ! [ -f ${MIGRATION_PATH} ]
then
  echo "File does not exist"
  echo "Possible Files:"
  for FILE in $("${FIND}" "${MIGRATION_FOLDER}" -iname "${FIND_SEARCH}")
  do
    echo - "$(echo "${FILE}" | rev | cut -d'/' -f1 | rev)"
  done
  exit 1
fi

echo "IMPORT MIGRATION FILE ${MIGRATION_FILE}"
"${NPX}" directus schema apply "${MIGRATION_PATH}"
echo "DONE"