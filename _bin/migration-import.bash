#!/bin/bash
DOCKER_COMPOSE="$(which docker-compose)"
CONTAINER="customer-data"
SCRIPT="/migrations/import.bash"

if [ -z "${DOCKER_COMPOSE}" ]
then
  DOCKER_COMPOSE="$(which docker) compose"
fi

"${DOCKER_COMPOSE}" exec "${CONTAINER}" "${SCRIPT}" "${1}"