#!/bin/bash
SCRIPT_PATH="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
SCHEMA_PATH="${SCRIPT_PATH}/../schemas"
INIT_FILE="${SCHEMA_PATH}/init.sql"
CURRENT_FILE=""

function export_current() {
  OUTPUT="$(${SCRIPT_PATH}/database-export.bash)"
  CURRENT_FILE="${SCHEMA_PATH}/$(echo ${OUTPUT} | awk '{print $4}')"
  if [ ! -f "${CURRENT_FILE}" ]
  then
    echo "ERROR: ${CURRENT_FILE} not found"
    exit 1
  fi
}

function current_to_init() {
  cp "${CURRENT_FILE}" "${INIT_FILE}"
}

echo "--> Export current Database"
export_current

echo "--> Copy current Database ${CURRENT_FILE} to ${INIT_FILE}"
current_to_init