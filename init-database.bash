#!/bin/bash
SCRIPT_PATH="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
LOCK_FILE="${SCRIPT_PATH}/database.lock"
INIT_FILE="init.sql"
IMPORT="${SCRIPT_PATH}/_bin/database-import.bash"
REDIS_CLI="${SCRIPT_PATH}/_bin/redis-cli"

if [ -f "${LOCK_FILE}" ]
then
  echo "${LOCK_FILE} exists"
  exit 1
fi

${IMPORT} ${INIT_FILE}
${REDIS_CLI} flushall

echo "INITIALIZED" > ${LOCK_FILE}