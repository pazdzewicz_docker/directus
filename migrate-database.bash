#!/bin/bash
SCRIPT_PATH="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
MIGRATE_FILE="latest.yml"
IMPORT="${SCRIPT_PATH}/_bin/migrate-import.bash"
REDIS_CLI="${SCRIPT_PATH}/_bin/redis-cli"

${IMPORT} ${MIGRATE_FILE}
${REDIS_CLI} flushall