#!/bin/bash
PG_DUMP="$(which pg_dump)"
CP="$(which cp)"

FILE_EXTENSION=".sql"

SCHEMA_FOLDER="/schemas/"
SCHEMA_FILE="export_$(date '+%Y-%m-%d_%H-%M-%S')${FILE_EXTENSION}"
SCHEMA_PATH="${SCHEMA_FOLDER}/${SCHEMA_FILE}"

LATEST_SCHEMA_FILE="latest${FILE_EXTENSION}"
LATEST_SCHEMA_PATH="${SCHEMA_FOLDER}/${LATEST_SCHEMA_FILE}"

echo "EXPORT SCHEMA FILE ${SCHEMA_FILE}"
${PG_DUMP} --host="localhost" --username="${POSTGRES_USER}" --dbname="${POSTGRES_DB}" > "${SCHEMA_PATH}"
${CP} "${SCHEMA_PATH}" "${LATEST_SCHEMA_PATH}"
echo "DONE"