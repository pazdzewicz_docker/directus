# directus

This Image contains directus and a few tools

## Parent image

- `directus/directus:${DIRECTUS_VERSION}`

## Dependencies

None

## Entrypoint & CMD

Since this Image is only used during CI we don't need an entrypoint.

## Functions

- `_bin/database-export.bash` - Export Database (useful for Backups)
- `_bin/database-import.bash` - Import Database (useful for Backup Restore)
- `_bin/migration-export.bash` - Export Migration (useful for Database Schema Changes)
- `_bin/migration-import.bash` - Import Migration (useful for Database Schema Changes)

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git. The Image will be tagged after your branch name.

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/directus:BRANCH
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "mydirectus:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- `DIRECTUS_VERSION` - The Directus Version to be installed, see https://github.com/directus/directus/releases

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:

## Ports

None

## Usage

### Local Usage

#### Docker Run

```
docker run registry.gitlab.com/pazdzewicz_docker/directus:latest
```

#### Docker Compose

***docker-compose.yml***

```
version: '3'

services:
  directus:
    image: registry.gitlab.com/pazdzewicz_docker/directus:latest
    build: .
    volumes:
      - "./:/etc/directus"
```

#### Gitlab CI
